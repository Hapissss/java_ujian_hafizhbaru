package com.ujianHafizh.ujian1;

public class ShowMonth {
    String bulanInput = "Mei";

    public void showMonth() {
        int mulaiHarike = 3;
        int spaces = mulaiHarike;
        String[] bulan = { "", // leave empty so that we start with months[1] = "January"
                "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Augstus", "September", "Oktober",
                "November", "Desember" };

        int[] jumlahHari = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

        switch (bulanInput) {
            case "Mei":
                System.out.println("==========Bulan " + bulanInput + "==========");
                System.out.println("   M    S    S    R    K    J    S");

                spaces = (jumlahHari[4 - 1] + spaces) % 7;

                for (int i = 0; i < spaces; i++)
                    System.out.print("     ");
                for (int i = 1; i <= jumlahHari[4]; i++) {
                    System.out.printf(" %3d ", i);
                    if (((i + spaces) % 7 == 0) || (i == jumlahHari[4]))
                        System.out.println();
                }
                break;

                
            default:
                break;
        }
    }
}