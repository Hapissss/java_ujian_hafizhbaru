package com.latihanHafizh.latihan1;

public class Siswa{
    protected KartuPelajar kartu;
    protected int uangJajan;
    protected int jamBelajar;
    protected int totalJajan;
    protected int uang;

    // Assigning KartuPelajar to Siswa
    public void setKartuPelajar(KartuPelajar kartu){
        this.kartu = kartu;
    }

    // Take a look what's inside KartuPelajar
    public KartuPelajar getKartu(){
        return this.kartu;
    }

    public int getTotalJajan(){
        return this.uangJajan * this.jamBelajar;
    }
}