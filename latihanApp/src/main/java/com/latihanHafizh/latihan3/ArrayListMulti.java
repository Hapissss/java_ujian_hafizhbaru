package com.latihanHafizh.latihan3;

import java.util.ArrayList;

public class ArrayListMulti {
    public void showArrayList(){
        ArrayList<ArrayList<Integer>> variableUtama = new ArrayList<>();

        ArrayList<Integer> v1 = new ArrayList<Integer>();
        ArrayList<Integer> v2 = new ArrayList<Integer>();
        ArrayList<Integer> v3 = new ArrayList<Integer>();


        v1.add(1);
        v1.add(2);
        v1.add(3);

        v2.add(4);
        v2.add(5);
        v2.add(6);

        v3.add(7);
        v3.add(8);
        v3.add(9);

        variableUtama.add(v1);
        variableUtama.add(v2);
        variableUtama.add(v3);

        System.out.println(variableUtama);


        // for(ArrayList obj:a){

        //     ArrayList<Integer> temp = obj;

        //     for(Integer job : temp){
        //         System.out.print(job+" ");
        //     }
        //     System.out.println();
        // }
    }
}